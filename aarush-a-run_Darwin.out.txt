*** Darwin 1x8 ***
Turn = 0.
  01234567
0 .tr.htft

Turn = 3.
  01234567
0 .rrh.ttt

Turn = 6.
  01234567
0 .rrr.ttt

Turn = 9.
  01234567
0 .rr.rttt

Turn = 12.
  01234567
0 r..rrrrr

Turn = 15.
  01234567
0 r.rr.rrr

Turn = 18.
  01234567
0 .rrrr.rr

Turn = 21.
  01234567
0 r.rrrrr.

*** Darwin 5x9 ***
Turn = 0.
  012345678
0 .t...f.r.
1 .h.......
2 .........
3 ....t....
4 ..f......

Turn = 2.
  012345678
0 .t...rr..
1 .t.......
2 .........
3 ....t....
4 ..f......

Turn = 4.
  012345678
0 .t...r...
1 .t....r..
2 .........
3 ....t....
4 ..f......

Turn = 6.
  012345678
0 .t.......
1 .t.......
2 .....r...
3 ....t.r..
4 ..f......

Turn = 8.
  012345678
0 .t.......
1 .t.......
2 .........
3 ....tt...
4 ..f...r..

Turn = 10.
  012345678
0 .t.......
1 .t.......
2 .........
3 ....tt...
4 ..f.r....

Turn = 12.
  012345678
0 .t.......
1 .t.......
2 .........
3 ....tt...
4 ..rr.....

Turn = 14.
  012345678
0 .t.......
1 .t.......
2 ..r......
3 ....tt...
4 .r.......

Turn = 16.
  012345678
0 .t.......
1 .tt......
2 .........
3 ....tt...
4 r........

Turn = 18.
  012345678
0 .t.......
1 .tt......
2 r........
3 ....tt...
4 .........

Turn = 20.
  012345678
0 rt.......
1 .tt......
2 .........
3 ....tt...
4 .........

Turn = 22.
  012345678
0 tt.......
1 .tt......
2 .........
3 ....tt...
4 .........

*** Darwin 9x10 ***
Turn = 0.
  0123456789
0 ..h.f....f
1 .......f..
2 ..........
3 ...rh....h
4 ..........
5 ........t.
6 ..........
7 .....r....
8 ..........

Turn = 4.
  0123456789
0 ...hf....f
1 ....h..f..
2 ..........
3 ..........
4 ..........
5 ........tt
6 ..........
7 ...r......
8 ...r......

Turn = 8.
  0123456789
0 ...hf....f
1 ....h..f..
2 ..........
3 ..........
4 ..........
5 ........tt
6 ..........
7 ......r...
8 r.........

Turn = 12.
  0123456789
0 ...hf....f
1 ....h..f..
2 ..........
3 ..........
4 r.........
5 ........tt
6 ..........
7 .........r
8 ..........

Turn = 16.
  0123456789
0 r..hf....f
1 ....h..f..
2 ..........
3 ..........
4 ..........
5 ........tt
6 ..........
7 ..........
8 .......r..

Turn = 20.
  0123456789
0 ..rrr....f
1 ....h..f..
2 ..........
3 ..........
4 ..........
5 ........tt
6 ..........
7 ..........
8 ...r......

*** Darwin 10x7 ***
Turn = 0.
  0123456
0 .......
1 ...t...
2 .....r.
3 .....f.
4 .......
5 .......
6 ...h...
7 .......
8 .......
9 .......

Turn = 5.
  0123456
0 ......r
1 ...t...
2 .......
3 .....f.
4 .......
5 .......
6 h......
7 .......
8 .......
9 .......

Turn = 10.
  0123456
0 .......
1 ...t...
2 .......
3 .....fr
4 .......
5 .......
6 h......
7 .......
8 .......
9 .......

Turn = 15.
  0123456
0 .......
1 ...t...
2 .......
3 .....f.
4 .......
5 .......
6 h......
7 .......
8 ......r
9 .......

Turn = 20.
  0123456
0 .......
1 ...t...
2 .......
3 .....f.
4 .......
5 .......
6 h......
7 .......
8 .......
9 ...r...

*** Darwin 8x2 ***
Turn = 0.
  01
0 .r
1 tt
2 r.
3 rr
4 ..
5 t.
6 ..
7 ..

Turn = 2.
  01
0 t.
1 tt
2 .r
3 ..
4 r.
5 tr
6 ..
7 ..

Turn = 4.
  01
0 t.
1 rr
2 .r
3 ..
4 r.
5 ..
6 r.
7 .r

Turn = 6.
  01
0 t.
1 .r
2 rr
3 r.
4 ..
5 ..
6 ..
7 rr

Turn = 8.
  01
0 t.
1 .r
2 rr
3 r.
4 ..
5 .r
6 r.
7 ..

Turn = 10.
  01
0 t.
1 .r
2 r.
3 rr
4 rr
5 ..
6 ..
7 ..

Turn = 12.
  01
0 t.
1 r.
2 .r
3 rr
4 r.
5 .r
6 ..
7 ..

Turn = 14.
  01
0 r.
1 r.
2 ..
3 rr
4 r.
5 .r
6 ..
7 .r

Turn = 16.
  01
0 r.
1 r.
2 r.
3 r.
4 ..
5 .r
6 .r
7 .r

Turn = 18.
  01
0 r.
1 ..
2 rr
3 ..
4 r.
5 r.
6 .r
7 r.

Turn = 20.
  01
0 ..
1 ..
2 r.
3 r.
4 rr
5 ..
6 r.
7 rr

*** Darwin 3x8 ***
Turn = 0.
  01234567
0 ....f...
1 r.......
2 ........

Turn = 1.
  01234567
0 r...f...
1 ........
2 ........

Turn = 2.
  01234567
0 r...f...
1 ........
2 ........

Turn = 3.
  01234567
0 .r..f...
1 ........
2 ........

Turn = 4.
  01234567
0 ..r.f...
1 ........
2 ........

Turn = 5.
  01234567
0 ...rf...
1 ........
2 ........

Turn = 6.
  01234567
0 ...r....
1 ....r...
2 ........

Turn = 7.
  01234567
0 ....r...
1 ........
2 ....r...

Turn = 8.
  01234567
0 .....r..
1 ........
2 ....r...

Turn = 9.
  01234567
0 ......r.
1 ........
2 .....r..

Turn = 10.
  01234567
0 .......r
1 ........
2 ......r.

Turn = 11.
  01234567
0 .......r
1 ........
2 .......r

Turn = 12.
  01234567
0 ........
1 .......r
2 .......r

Turn = 13.
  01234567
0 ........
1 .......r
2 .......r

Turn = 14.
  01234567
0 ........
1 ......r.
2 ......r.

Turn = 15.
  01234567
0 ........
1 .....r..
2 .....r..

Turn = 16.
  01234567
0 ........
1 ....r...
2 ....r...

Turn = 17.
  01234567
0 ........
1 ...r....
2 ...r....

Turn = 18.
  01234567
0 ........
1 ..r.....
2 ..r.....

Turn = 19.
  01234567
0 ........
1 .r......
2 .r......

Turn = 20.
  01234567
0 ........
1 r.......
2 r.......

Turn = 21.
  01234567
0 ........
1 r.......
2 r.......

Turn = 22.
  01234567
0 ........
1 r.......
2 r.......

Turn = 23.
  01234567
0 ........
1 r.......
2 r.......

*** Darwin 4x5 ***
Turn = 0.
  01234
0 .....
1 ..h..
2 .....
3 .....

Turn = 2.
  01234
0 ..h..
1 .....
2 .....
3 .....

Turn = 4.
  01234
0 ..h..
1 .....
2 .....
3 .....

Turn = 6.
  01234
0 ..h..
1 .....
2 .....
3 .....

Turn = 8.
  01234
0 ..h..
1 .....
2 .....
3 .....

Turn = 10.
  01234
0 ..h..
1 .....
2 .....
3 .....

Turn = 12.
  01234
0 ..h..
1 .....
2 .....
3 .....

Turn = 14.
  01234
0 ..h..
1 .....
2 .....
3 .....

Turn = 16.
  01234
0 ..h..
1 .....
2 .....
3 .....

Turn = 18.
  01234
0 ..h..
1 .....
2 .....
3 .....

Turn = 20.
  01234
0 ..h..
1 .....
2 .....
3 .....

Turn = 22.
  01234
0 ..h..
1 .....
2 .....
3 .....

*** Darwin 5x2 ***
Turn = 0.
  01
0 ..
1 ..
2 .h
3 .h
4 ..

Turn = 1.
  01
0 ..
1 .h
2 ..
3 ..
4 .h

Turn = 2.
  01
0 .h
1 ..
2 ..
3 ..
4 .h

Turn = 3.
  01
0 .h
1 ..
2 ..
3 ..
4 .h

Turn = 4.
  01
0 .h
1 ..
2 ..
3 ..
4 .h

Turn = 5.
  01
0 .h
1 ..
2 ..
3 ..
4 .h

Turn = 6.
  01
0 .h
1 ..
2 ..
3 ..
4 .h

Turn = 7.
  01
0 .h
1 ..
2 ..
3 ..
4 .h

Turn = 8.
  01
0 .h
1 ..
2 ..
3 ..
4 .h

Turn = 9.
  01
0 .h
1 ..
2 ..
3 ..
4 .h

Turn = 10.
  01
0 .h
1 ..
2 ..
3 ..
4 .h

Turn = 11.
  01
0 .h
1 ..
2 ..
3 ..
4 .h

Turn = 12.
  01
0 .h
1 ..
2 ..
3 ..
4 .h

Turn = 13.
  01
0 .h
1 ..
2 ..
3 ..
4 .h

Turn = 14.
  01
0 .h
1 ..
2 ..
3 ..
4 .h

Turn = 15.
  01
0 .h
1 ..
2 ..
3 ..
4 .h

Turn = 16.
  01
0 .h
1 ..
2 ..
3 ..
4 .h

Turn = 17.
  01
0 .h
1 ..
2 ..
3 ..
4 .h

Turn = 18.
  01
0 .h
1 ..
2 ..
3 ..
4 .h

Turn = 19.
  01
0 .h
1 ..
2 ..
3 ..
4 .h

Turn = 20.
  01
0 .h
1 ..
2 ..
3 ..
4 .h

Turn = 21.
  01
0 .h
1 ..
2 ..
3 ..
4 .h

Turn = 22.
  01
0 .h
1 ..
2 ..
3 ..
4 .h

Turn = 23.
  01
0 .h
1 ..
2 ..
3 ..
4 .h

*** Darwin 5x7 ***
Turn = 0.
  0123456
0 .......
1 .......
2 .......
3 hf.....
4 .....rt

Turn = 3.
  0123456
0 h......
1 .....r.
2 .......
3 .f.....
4 ......t

Turn = 6.
  0123456
0 h.....r
1 .......
2 .......
3 .f.....
4 ......t

Turn = 9.
  0123456
0 h.....r
1 .......
2 .......
3 .f.....
4 ......t

Turn = 12.
  0123456
0 h......
1 .......
2 .......
3 .f....t
4 ......t

Turn = 15.
  0123456
0 h......
1 .......
2 .......
3 .f....t
4 ......t

Turn = 18.
  0123456
0 h......
1 .......
2 .......
3 .f....t
4 ......t

Turn = 21.
  0123456
0 h......
1 .......
2 .......
3 .f....t
4 ......t

*** Darwin 9x8 ***
Turn = 0.
  01234567
0 ........
1 ........
2 ........
3 ........
4 .r......
5 ........
6 ........
7 ........
8 ........

Turn = 3.
  01234567
0 ........
1 ........
2 ........
3 r.......
4 ........
5 ........
6 ........
7 ........
8 ........

Turn = 6.
  01234567
0 r.......
1 ........
2 ........
3 ........
4 ........
5 ........
6 ........
7 ........
8 ........

Turn = 9.
  01234567
0 r.......
1 ........
2 ........
3 ........
4 ........
5 ........
6 ........
7 ........
8 ........

Turn = 12.
  01234567
0 ...r....
1 ........
2 ........
3 ........
4 ........
5 ........
6 ........
7 ........
8 ........

Turn = 15.
  01234567
0 ......r.
1 ........
2 ........
3 ........
4 ........
5 ........
6 ........
7 ........
8 ........

Turn = 18.
  01234567
0 ........
1 .......r
2 ........
3 ........
4 ........
5 ........
6 ........
7 ........
8 ........

Turn = 21.
  01234567
0 ........
1 ........
2 ........
3 ........
4 .......r
5 ........
6 ........
7 ........
8 ........

*** Darwin 3x2 ***
Turn = 0.
  01
0 f.
1 t.
2 t.

Turn = 2.
  01
0 t.
1 t.
2 t.

Turn = 4.
  01
0 t.
1 t.
2 t.

Turn = 6.
  01
0 t.
1 t.
2 t.

Turn = 8.
  01
0 t.
1 t.
2 t.

Turn = 10.
  01
0 t.
1 t.
2 t.

Turn = 12.
  01
0 t.
1 t.
2 t.

Turn = 14.
  01
0 t.
1 t.
2 t.

Turn = 16.
  01
0 t.
1 t.
2 t.

Turn = 18.
  01
0 t.
1 t.
2 t.

Turn = 20.
  01
0 t.
1 t.
2 t.

Turn = 22.
  01
0 t.
1 t.
2 t.

*** Darwin 9x9 ***
Turn = 0.
  012345678
0 ...f.....
1 ....t....
2 .r.......
3 .........
4 ......r..
5 .........
6 .........
7 ...r.....
8 .....r.fr

Turn = 2.
  012345678
0 ...f.....
1 ....t....
2 r.....r..
3 .........
4 .........
5 .........
6 .....r...
7 .r.......
8 ......rr.

Turn = 4.
  012345678
0 r..f..r..
1 ....t....
2 .........
3 .........
4 .....r...
5 .........
6 .........
7 r........
8 ....rr...

Turn = 6.
  012345678
0 .r.f...r.
1 ....t....
2 .....r...
3 .........
4 .........
5 .........
6 .........
7 .........
8 r.rr.....

Turn = 8.
  012345678
0 ..r.r...r
1 ....tt...
2 .........
3 .........
4 .........
5 .........
6 .........
7 r........
8 rr.......

Turn = 10.
  012345678
0 ....t.rr.
1 ....tt...
2 .........
3 .........
4 .........
5 r........
6 .........
7 rr.......
8 .........

Turn = 12.
  012345678
0 ....t.rr.
1 ....tt...
2 .........
3 r........
4 .........
5 rr.......
6 .........
7 .........
8 .........

Turn = 14.
  012345678
0 ....rr..r
1 r...tt...
2 .........
3 rr.......
4 .........
5 .........
6 .........
7 .........
8 .........

Turn = 16.
  012345678
0 r...tr...
1 rr..tr...
2 ........r
3 .........
4 .........
5 .........
6 .........
7 .........
8 .........

Turn = 18.
  012345678
0 rr..rr...
1 r...rr...
2 .........
3 .........
4 ........r
5 .........
6 .........
7 .........
8 .........

Turn = 20.
  012345678
0 ...rrr...
1 r....r...
2 ....r....
3 r........
4 .........
5 .........
6 ........r
7 .........
8 .........

Turn = 22.
  012345678
0 ...rrr...
1 .........
2 ....r....
3 r........
4 ....r....
5 r........
6 .........
7 .........
8 ........r

*** Darwin 4x1 ***
Turn = 0.
  0
0 r
1 h
2 f
3 h

Turn = 3.
  0
0 r
1 r
2 r
3 r

Turn = 6.
  0
0 r
1 r
2 r
3 r

Turn = 9.
  0
0 r
1 r
2 r
3 r

Turn = 12.
  0
0 r
1 r
2 r
3 r

Turn = 15.
  0
0 r
1 r
2 r
3 r

Turn = 18.
  0
0 r
1 r
2 r
3 r

Turn = 21.
  0
0 r
1 r
2 r
3 r

Turn = 24.
  0
0 r
1 r
2 r
3 r

*** Darwin 6x3 ***
Turn = 0.
  012
0 ...
1 r..
2 r..
3 ...
4 f..
5 h.f

Turn = 5.
  012
0 ...
1 .r.
2 ..r
3 ...
4 f..
5 .hf

Turn = 10.
  012
0 r..
1 ...
2 ...
3 ...
4 fr.
5 .hr

Turn = 15.
  012
0 ...
1 .r.
2 ...
3 rr.
4 .r.
5 ..r

Turn = 20.
  012
0 rrr
1 r..
2 ...
3 r..
4 ...
5 ...

*** Darwin 6x10 ***
Turn = 0.
  0123456789
0 .....r....
1 ....f.....
2 ....h.....
3 .......h..
4 ..........
5 ..........

Turn = 1.
  0123456789
0 .....r....
1 ....f.....
2 ....h..h..
3 ..........
4 ..........
5 ..........

Turn = 2.
  0123456789
0 ......r...
1 ....f..h..
2 ....h.....
3 ..........
4 ..........
5 ..........

Turn = 3.
  0123456789
0 .......r..
1 ....f..h..
2 ....h.....
3 ..........
4 ..........
5 ..........

Turn = 4.
  0123456789
0 .......hr.
1 ....f.....
2 ....h.....
3 ..........
4 ..........
5 ..........

Turn = 5.
  0123456789
0 .......h.r
1 ....f.....
2 ....h.....
3 ..........
4 ..........
5 ..........

Turn = 6.
  0123456789
0 .......h.r
1 ....f.....
2 ....h.....
3 ..........
4 ..........
5 ..........

Turn = 7.
  0123456789
0 .......h.r
1 ....f.....
2 ....h.....
3 ..........
4 ..........
5 ..........

Turn = 8.
  0123456789
0 .......h.r
1 ....f.....
2 ....h.....
3 ..........
4 ..........
5 ..........

Turn = 9.
  0123456789
0 .......h..
1 ....f....r
2 ....h.....
3 ..........
4 ..........
5 ..........

Turn = 10.
  0123456789
0 .......h..
1 ....f.....
2 ....h....r
3 ..........
4 ..........
5 ..........

Turn = 11.
  0123456789
0 .......h..
1 ....f.....
2 ....h.....
3 .........r
4 ..........
5 ..........

Turn = 12.
  0123456789
0 .......h..
1 ....f.....
2 ....h.....
3 ..........
4 .........r
5 ..........

Turn = 13.
  0123456789
0 .......h..
1 ....f.....
2 ....h.....
3 ..........
4 ..........
5 .........r

Turn = 14.
  0123456789
0 .......h..
1 ....f.....
2 ....h.....
3 ..........
4 ..........
5 .........r

Turn = 15.
  0123456789
0 .......h..
1 ....f.....
2 ....h.....
3 ..........
4 ..........
5 ........r.

Turn = 16.
  0123456789
0 .......h..
1 ....f.....
2 ....h.....
3 ..........
4 ..........
5 .......r..

Turn = 17.
  0123456789
0 .......h..
1 ....f.....
2 ....h.....
3 ..........
4 ..........
5 ......r...

Turn = 18.
  0123456789
0 .......h..
1 ....f.....
2 ....h.....
3 ..........
4 ..........
5 .....r....

Turn = 19.
  0123456789
0 .......h..
1 ....f.....
2 ....h.....
3 ..........
4 ..........
5 ....r.....

Turn = 20.
  0123456789
0 .......h..
1 ....f.....
2 ....h.....
3 ..........
4 ..........
5 ...r......

Turn = 21.
  0123456789
0 .......h..
1 ....f.....
2 ....h.....
3 ..........
4 ..........
5 ..r.......

Turn = 22.
  0123456789
0 .......h..
1 ....f.....
2 ....h.....
3 ..........
4 ..........
5 .r........

Turn = 23.
  0123456789
0 .......h..
1 ....f.....
2 ....h.....
3 ..........
4 ..........
5 r.........

*** Darwin 8x7 ***
Turn = 0.
  0123456
0 ....f..
1 ..r....
2 .f.....
3 .r.....
4 .......
5 ..h....
6 .....f.
7 .......

Turn = 5.
  0123456
0 r...f..
1 .......
2 .f.....
3 .......
4 .......
5 ......h
6 .....f.
7 .r.....

Turn = 10.
  0123456
0 ...rf..
1 .......
2 .f.....
3 .......
4 r......
5 ......h
6 .....f.
7 .......

Turn = 15.
  0123456
0 r.....r
1 .......
2 .f.....
3 .......
4 .......
5 ....r.h
6 .....f.
7 .......

Turn = 20.
  0123456
0 .......
1 .......
2 rf.....
3 ......r
4 .......
5 ......h
6 .....f.
7 ..r....

*** Darwin 6x10 ***
Turn = 0.
  0123456789
0 .f........
1 ..f.......
2 ..t......f
3 r.........
4 .....t....
5 f..r......

Turn = 5.
  0123456789
0 rf.r......
1 ..t.......
2 ..t......f
3 ..........
4 .....t....
5 f.........

Turn = 10.
  0123456789
0 ..r.r..r..
1 ..t.......
2 ..t......f
3 ..........
4 .....t....
5 f.........

Turn = 15.
  0123456789
0 .......r.r
1 ..t......r
2 ..t.......
3 .........r
4 .....t....
5 f.........

Turn = 20.
  0123456789
0 ........rr
1 ..t.......
2 ..t.......
3 ..........
4 .....t..r.
5 f......r..

*** Darwin 10x3 ***
Turn = 0.
  012
0 ...
1 ...
2 ...
3 ...
4 ..f
5 ...
6 h..
7 h..
8 ...
9 ...

Turn = 1.
  012
0 ...
1 ...
2 ...
3 ...
4 ..f
5 ...
6 h..
7 ...
8 h..
9 ...

Turn = 2.
  012
0 ...
1 ...
2 ...
3 ...
4 ..f
5 ...
6 ...
7 h..
8 ...
9 h..

Turn = 3.
  012
0 ...
1 ...
2 ...
3 ...
4 ..f
5 ...
6 ...
7 ...
8 h..
9 h..

Turn = 4.
  012
0 ...
1 ...
2 ...
3 ...
4 ..f
5 ...
6 ...
7 ...
8 h..
9 h..

Turn = 5.
  012
0 ...
1 ...
2 ...
3 ...
4 ..f
5 ...
6 ...
7 ...
8 h..
9 h..

Turn = 6.
  012
0 ...
1 ...
2 ...
3 ...
4 ..f
5 ...
6 ...
7 ...
8 h..
9 h..

Turn = 7.
  012
0 ...
1 ...
2 ...
3 ...
4 ..f
5 ...
6 ...
7 ...
8 h..
9 h..

Turn = 8.
  012
0 ...
1 ...
2 ...
3 ...
4 ..f
5 ...
6 ...
7 ...
8 h..
9 h..

Turn = 9.
  012
0 ...
1 ...
2 ...
3 ...
4 ..f
5 ...
6 ...
7 ...
8 h..
9 h..

Turn = 10.
  012
0 ...
1 ...
2 ...
3 ...
4 ..f
5 ...
6 ...
7 ...
8 h..
9 h..

Turn = 11.
  012
0 ...
1 ...
2 ...
3 ...
4 ..f
5 ...
6 ...
7 ...
8 h..
9 h..

Turn = 12.
  012
0 ...
1 ...
2 ...
3 ...
4 ..f
5 ...
6 ...
7 ...
8 h..
9 h..

Turn = 13.
  012
0 ...
1 ...
2 ...
3 ...
4 ..f
5 ...
6 ...
7 ...
8 h..
9 h..

Turn = 14.
  012
0 ...
1 ...
2 ...
3 ...
4 ..f
5 ...
6 ...
7 ...
8 h..
9 h..

Turn = 15.
  012
0 ...
1 ...
2 ...
3 ...
4 ..f
5 ...
6 ...
7 ...
8 h..
9 h..

Turn = 16.
  012
0 ...
1 ...
2 ...
3 ...
4 ..f
5 ...
6 ...
7 ...
8 h..
9 h..

Turn = 17.
  012
0 ...
1 ...
2 ...
3 ...
4 ..f
5 ...
6 ...
7 ...
8 h..
9 h..

Turn = 18.
  012
0 ...
1 ...
2 ...
3 ...
4 ..f
5 ...
6 ...
7 ...
8 h..
9 h..

Turn = 19.
  012
0 ...
1 ...
2 ...
3 ...
4 ..f
5 ...
6 ...
7 ...
8 h..
9 h..

Turn = 20.
  012
0 ...
1 ...
2 ...
3 ...
4 ..f
5 ...
6 ...
7 ...
8 h..
9 h..

Turn = 21.
  012
0 ...
1 ...
2 ...
3 ...
4 ..f
5 ...
6 ...
7 ...
8 h..
9 h..

Turn = 22.
  012
0 ...
1 ...
2 ...
3 ...
4 ..f
5 ...
6 ...
7 ...
8 h..
9 h..

Turn = 23.
  012
0 ...
1 ...
2 ...
3 ...
4 ..f
5 ...
6 ...
7 ...
8 h..
9 h..

*** Darwin 10x8 ***
Turn = 0.
  01234567
0 .....h..
1 ..hh....
2 ....r...
3 .t......
4 ........
5 .....r.r
6 ........
7 .......t
8 ...f...f
9 ........

Turn = 2.
  01234567
0 ..h....h
1 .h......
2 ........
3 .t......
4 ....r..r
5 ......r.
6 ........
7 .......t
8 ...f...f
9 ........

Turn = 4.
  01234567
0 ..h....h
1 h.......
2 .......r
3 .t....r.
4 ........
5 ........
6 ....r...
7 .......t
8 ...f...t
9 ........

Turn = 6.
  01234567
0 ..h....r
1 h.....rr
2 ........
3 .t......
4 ........
5 ........
6 ........
7 .......t
8 ...fr..t
9 ........

Turn = 8.
  01234567
0 ..h...rr
1 h......r
2 ........
3 .t......
4 ........
5 ........
6 ........
7 .......t
8 ...f...t
9 ....r...

Turn = 10.
  01234567
0 ..h....r
1 h.....rr
2 ........
3 .t......
4 ........
5 ........
6 ........
7 .......t
8 ...f...t
9 ......r.

Turn = 12.
  01234567
0 ..h.....
1 h......r
2 ........
3 .t....rr
4 ........
5 ........
6 ........
7 .......t
8 ...f...t
9 .......r

Turn = 14.
  01234567
0 ..h.....
1 h.......
2 ........
3 .t.....r
4 ........
5 ......rr
6 ........
7 .......t
8 ...f...t
9 .......t

Turn = 16.
  01234567
0 ..h.....
1 h.......
2 ........
3 .t......
4 ........
5 .......r
6 .......t
7 ......rt
8 ...f...t
9 .......t

Turn = 18.
  01234567
0 ..h.....
1 h.......
2 ........
3 .t......
4 ........
5 .......r
6 .......r
7 .......t
8 ...f..tt
9 .......t

Turn = 20.
  01234567
0 ..h.....
1 h.......
2 ........
3 .t......
4 .......r
5 ........
6 .....r..
7 .......t
8 ...f..tt
9 .......t

*** Darwin 4x2 ***
Turn = 0.
  01
0 t.
1 h.
2 .h
3 .t

Turn = 4.
  01
0 t.
1 t.
2 t.
3 .t

Turn = 8.
  01
0 t.
1 t.
2 t.
3 .t

Turn = 12.
  01
0 t.
1 t.
2 t.
3 .t

Turn = 16.
  01
0 t.
1 t.
2 t.
3 .t

Turn = 20.
  01
0 t.
1 t.
2 t.
3 .t

*** Darwin 2x4 ***
Turn = 0.
  0123
0 .f.h
1 ....

Turn = 4.
  0123
0 .fh.
1 ....

Turn = 8.
  0123
0 .fh.
1 ....

Turn = 12.
  0123
0 .fh.
1 ....

Turn = 16.
  0123
0 .fh.
1 ....

Turn = 20.
  0123
0 .fh.
1 ....

*** Darwin 10x4 ***
Turn = 0.
  0123
0 ....
1 .t..
2 ....
3 ....
4 ....
5 ....
6 ....
7 ....
8 ....
9 ....

Turn = 2.
  0123
0 ....
1 .t..
2 ....
3 ....
4 ....
5 ....
6 ....
7 ....
8 ....
9 ....

Turn = 4.
  0123
0 ....
1 .t..
2 ....
3 ....
4 ....
5 ....
6 ....
7 ....
8 ....
9 ....

Turn = 6.
  0123
0 ....
1 .t..
2 ....
3 ....
4 ....
5 ....
6 ....
7 ....
8 ....
9 ....

Turn = 8.
  0123
0 ....
1 .t..
2 ....
3 ....
4 ....
5 ....
6 ....
7 ....
8 ....
9 ....

Turn = 10.
  0123
0 ....
1 .t..
2 ....
3 ....
4 ....
5 ....
6 ....
7 ....
8 ....
9 ....

Turn = 12.
  0123
0 ....
1 .t..
2 ....
3 ....
4 ....
5 ....
6 ....
7 ....
8 ....
9 ....

Turn = 14.
  0123
0 ....
1 .t..
2 ....
3 ....
4 ....
5 ....
6 ....
7 ....
8 ....
9 ....

Turn = 16.
  0123
0 ....
1 .t..
2 ....
3 ....
4 ....
5 ....
6 ....
7 ....
8 ....
9 ....

Turn = 18.
  0123
0 ....
1 .t..
2 ....
3 ....
4 ....
5 ....
6 ....
7 ....
8 ....
9 ....

Turn = 20.
  0123
0 ....
1 .t..
2 ....
3 ....
4 ....
5 ....
6 ....
7 ....
8 ....
9 ....
